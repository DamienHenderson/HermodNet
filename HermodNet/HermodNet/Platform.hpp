#pragma once

#if defined(_WIN32)
#define HERMOD_NET_PLATFORM_WINDOWS

#include <WS2tcpip.h>
#include <WinSock2.h>


#define HERMOD_NET_SOCKET_HANDLE SOCKET
#define HERMOD_NET_INVALID_SOCKET INVALID_SOCKET
#elif defined(__linux__)
#define HERMOD_NET_PLATFORM_LINUX
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <netdb.h>
#include <unistd.h>

#define HERMOD_NET_SOCKET_HANDLE int
#define HERMOD_NET_INVALID_SOCKET 0
#endif
