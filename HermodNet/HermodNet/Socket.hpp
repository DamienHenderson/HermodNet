#pragma once

#include <string>
#include <cstdint>

#include "Platform.hpp"

namespace HermodNet
{
	// if you want to change the amount you can receive then unfortunately you'll need to change this and recompile as the buffer array size must be known at compile time
	static constexpr size_t buffer_size = 2048;

	class Socket
	{
	public:
		Socket(const std::string& host, uint16_t port);
		virtual ~Socket();

		void Send(const std::string& data);

		void Send(const char* data, size_t length);

		std::string Receive();

		// receive into a char pointer, max size should be the number of bytes available in the pointer
		// this is to prevent a buffer overrun
		void Receive(char* ptr, size_t max_size);

		// Returns the OS socket handle on windows this is a SOCKET on linux this is an int
		// This should only be used when you have to do something platform specific
		HERMOD_NET_SOCKET_HANDLE GetOSSocketHandle() const { return socket_handle_; };
	private:

		HERMOD_NET_SOCKET_HANDLE socket_handle_ = HERMOD_NET_INVALID_SOCKET;
	};

}
