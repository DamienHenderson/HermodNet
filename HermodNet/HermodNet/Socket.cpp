#include "Socket.hpp"

#include <iostream>
#include <sstream>

namespace HermodNet
{


	


	Socket::Socket(const std::string & host, uint16_t port)
	{

#if defined(HERMOD_NET_PLATFORM_WINDOWS)
		std::stringstream ss;
		ss << port;
		std::string port_str = ss.str();
		addrinfo* addr = nullptr;
		if (getaddrinfo(host.c_str(), port_str.c_str(), nullptr, &addr) != 0)
		{
			std::cout << "Cannot get address info\n";
		}

		socket_handle_ = socket(addr->ai_family, addr->ai_socktype, addr->ai_protocol);

		int conn_result = connect(socket_handle_, addr->ai_addr, (int)addr->ai_addrlen);
#elif defined(HERMOD_NET_PLATFORM_LINUX)
		// TODO: linux implementation with bsd sockets
#endif
	}

	Socket::~Socket()
	{
	}

	void Socket::Send(const std::string & data)
	{
#if defined(HERMOD_NET_PLATFORM_WINDOWS)
		send(socket_handle_, data.c_str(), data.length(), 0);
#endif
	}

	void Socket::Send(const char * data, size_t length)
	{
		send(socket_handle_, data, length, 0);
	}

	std::string Socket::Receive()
	{
		char buffer[buffer_size];
		std::string out = "";
#if defined(HERMOD_NET_PLATFORM_WINDOWS)
		int received = recv(socket_handle_, buffer, buffer_size, 0);
		out = std::string(buffer, received);
#endif

		return out;
	}

	void Socket::Receive(char * ptr, size_t max_size)
	{
#if defined(HERMOD_NET_PLATFORM_WINDOWS)
		int received = recv(socket_handle_, ptr, max_size, 0);
		
#endif
	}

}
