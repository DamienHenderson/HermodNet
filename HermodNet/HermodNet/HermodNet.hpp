#pragma once

#include "Platform.hpp"

namespace HermodNet
{
	bool Init();

	void Shutdown();
}

namespace hnet = HermodNet;