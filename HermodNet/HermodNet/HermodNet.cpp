#include "HermodNet.hpp"

#include <iostream>

#if defined(HERMOD_NET_PLATFORM_WINDOWS)
#include <WS2tcpip.h>
#include <WinSock2.h>
#elif defined(HERMOD_NET_PLATFORM_LINUX)
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <netdb.h>
#include <unistd.h>
#endif

bool HermodNet::Init()
{
#if defined(HERMOD_NET_PLATFORM_WINDOWS)
	WSAData data;
	if (WSAStartup(MAKEWORD(2, 2), &data) != 0)
	{
		std::cout << "Winsock startup failed\n";
		return false;
	}
	return true;
#endif
}

void HermodNet::Shutdown()
{
#if defined(HERMOD_NET_PLATFORM_WINDOWS)
	WSACleanup();

#endif
}
